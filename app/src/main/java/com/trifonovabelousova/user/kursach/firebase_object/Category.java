package com.trifonovabelousova.user.kursach.firebase_object;

/**
 * Категория продукта - мясо, молочная продукция и т.д.
 */

public class Category {

    public long id; //идентификатор для выбора и поиска
    public String title; //Название категории

    public Category() {
    }

    public Category(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public long getId() {
        return id;
    }
}