package com.trifonovabelousova.user.kursach.fragment;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.adapter.ProductNeedListAdapter;
import com.trifonovabelousova.user.kursach.firebase_object.MyProduct;
import com.trifonovabelousova.user.kursach.firebase_object.NeedProduct;
import com.trifonovabelousova.user.kursach.firebase_object.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class productneed extends Fragment {

    RecyclerView rv;
    FloatingActionButton pl;
    FirebaseDatabase database;
    DatabaseReference reference;
    DatabaseReference reference_product;
    List<Product> listProducts=new ArrayList<>();
    ProductNeedListAdapter productListAdapter;
    private FirebaseAuth mAuth; //переменная для того чтобы получить текущего пользователя
    private FirebaseUser user; //переменная для того чтобы получить текущего пользователя
    private HashMap<Long,Long> ids=new HashMap<>();

    ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
            final NeedProduct needProduct=dataSnapshot.getValue(NeedProduct.class);
            ids.put(needProduct.id_product,needProduct.id);
            reference_product.child(String.valueOf(needProduct.id_product)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Product product=dataSnapshot.getValue(Product.class);
                    product.description=needProduct.weight;
                    productListAdapter.addItem(product);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            final NeedProduct needProduct=dataSnapshot.getValue(NeedProduct.class);
            productListAdapter.removeItemId(needProduct.id_product);
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b) {
        View v = li.inflate(R.layout.fragment_product, vg, false);

        rv = (RecyclerView) v.findViewById(R.id.rv);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        productListAdapter=new ProductNeedListAdapter(listProducts, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Product p=(Product)v.getTag();
                reference.child(String.valueOf(ids.get(p.id))).removeValue();
            }
        });
        rv.setAdapter(productListAdapter);

        mAuth = FirebaseAuth.getInstance();
        user=mAuth.getCurrentUser();
        database = FirebaseDatabase.getInstance();
        if (user == null)
            return v;
        reference = database.getReference("user_need_product").child(user.getUid());
        reference_product = database.getReference("product");


        Query recentPostsQuery = reference.limitToLast(100); //	Устанавливает максимальное количество элементов для возврата из конца упорядоченного списка результатов

        recentPostsQuery.addChildEventListener(childEventListener);



        pl = (FloatingActionButton) v.findViewById(R.id.plus);
        pl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b=new Bundle();
                b.putInt("regim",1);
                cat p=new cat();
                p.setArguments(b);
                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.frame, p, "text").addToBackStack(null).commit();
            }
        });

        return v;
    }
}