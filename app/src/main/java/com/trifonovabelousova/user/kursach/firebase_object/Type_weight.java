package com.trifonovabelousova.user.kursach.firebase_object;

/**
 * Тип учета веса - литры, кг и т.д.
 */

public class Type_weight {

    public long id; //идентификатор для выбора и поиска
    public String title; //наименование: литры, килограммы ...
    public String shorttitle; //наименование: л, кг, гр ...
    public float min_weight; //минимальный вес для выбор, например 0.1, 1, 10

    public Type_weight() {
    }

    public Type_weight(String title, String shorttitle, float min_weight) {
        this.title = title;
        this.shorttitle = shorttitle;
        this.min_weight = min_weight;
    }

}