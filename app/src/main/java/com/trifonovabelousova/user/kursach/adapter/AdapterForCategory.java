package com.trifonovabelousova.user.kursach.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.firebase_object.Category;

import java.util.List;

/**
 * Created by user on 13.11.2017.
 */

public class AdapterForCategory extends BaseAdapter<Category,AdapterForCategory.CategoryViewHolder> {

    public static class CategoryViewHolder extends BaseAdapter.ViewHolder {

        RelativeLayout cv;
        TextView a1;
        TextView a2;
        ImageView internetUrl;

        CategoryViewHolder(View itemView) {
            super(itemView);
            cv = (RelativeLayout) itemView.findViewById(R.id.cv);
            a1 = (TextView) itemView.findViewById(R.id.a1);
            a2 = (TextView) itemView.findViewById(R.id.a2);
            internetUrl = (ImageView) itemView.findViewById(R.id.image);
        }
    }

    public AdapterForCategory(List<Category> cl) {
        super(cl);
    }



    @Override
    public View onCreateView(ViewGroup parent, int viewType) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(View itemView) {
        return new CategoryViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final AdapterForCategory.CategoryViewHolder holder, final int i) {
        super.onBindViewHolder(holder, i);
        holder.a1.setText(getItem(i).getTitle());
    }
}
