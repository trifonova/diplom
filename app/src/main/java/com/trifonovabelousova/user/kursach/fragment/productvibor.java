package com.trifonovabelousova.user.kursach.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.activity.NewReceptActivity;
import com.trifonovabelousova.user.kursach.adapter.AdapterForUserProduct;
import com.trifonovabelousova.user.kursach.firebase_object.Dish_product;
import com.trifonovabelousova.user.kursach.firebase_object.MyProduct;
import com.trifonovabelousova.user.kursach.firebase_object.NeedProduct;
import com.trifonovabelousova.user.kursach.firebase_object.Product;

import java.util.ArrayList;
import java.util.List;

public class productvibor extends Fragment {

    RecyclerView rv;
    FirebaseDatabase database;
    DatabaseReference reference;
    List<Product> listProducts=new ArrayList<>();
    AdapterForUserProduct productListAdapter;
    private FirebaseAuth mAuth; //переменная для того чтобы получить текущего пользователя
    private FirebaseUser user; //переменная для того чтобы получить текущего пользователя
    long id;

    int regim=0; //0 - выбор, что имеем в холодильнике, 1 - выбор, что должно быть в холодильнике

    ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
            if (dataSnapshot.getValue(Product.class).category.id==id || id==-1) {
                listProducts.add(dataSnapshot.getValue(Product.class));
                productListAdapter = new AdapterForUserProduct(listProducts);
                rv.setAdapter(productListAdapter);
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            productListAdapter.removeItem(dataSnapshot.getValue(Product.class));
            productListAdapter=new AdapterForUserProduct(listProducts);
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    ChildEventListener childEventListenerHave = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
            productListAdapter.addHave(dataSnapshot.getValue(Product.class));
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            productListAdapter.removeItem(dataSnapshot.getValue(Product.class));
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b) {
        View v = li.inflate(R.layout.fragment_product, vg, false);

        rv = (RecyclerView) v.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        if (getArguments()!=null) {
            id = getArguments().getLong("id", 0);
            regim = getArguments().getInt("regim", 0);
        } else
            id=-1;


        productListAdapter=new AdapterForUserProduct(listProducts);
        rv.setAdapter(productListAdapter);

        mAuth = FirebaseAuth.getInstance();
        user=mAuth.getCurrentUser();
        database = FirebaseDatabase.getInstance();
        reference = database.getReference("product");


        Query recentPostsQuery = reference.limitToLast(100); //	Устанавливает максимальное количество элементов для возврата из конца упорядоченного списка результатов

        recentPostsQuery.addChildEventListener(childEventListener);

        v.findViewById(R.id.plus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(user!=null) {
                    if(NewReceptActivity.dish==null) {
                        if(regim==0) {
                            reference = database.getReference("user_product").child(user.getUid());
                            for (Product product : productListAdapter.getSelect()) {
                                MyProduct myProduct = new MyProduct();
                                myProduct.id = product.id;
                                myProduct.id_product = product.id;
                                myProduct.weight = product.description;
                                reference.child(String.valueOf(myProduct.id)).setValue(myProduct);
                            }
                        }
                        else{
                            reference = database.getReference("user_need_product").child(user.getUid());
                            for (Product product : productListAdapter.getSelect()) {
                                NeedProduct myProduct = new NeedProduct();
                                myProduct.id = product.id;
                                myProduct.id_product = product.id;
                                myProduct.weight = product.description;
                                reference.child(String.valueOf(myProduct.id)).setValue(myProduct);
                            }
                        }
                        Toast.makeText(getActivity(), "Продукты сохранены в каталог", Toast.LENGTH_LONG).show();
                    }
                    else{
                        NewReceptActivity.dish.product=new ArrayList<Dish_product>();
                        for (Product product : productListAdapter.getSelect()) {
                            Dish_product dish_product = new Dish_product();
                            dish_product.id = System.currentTimeMillis();
                            dish_product.weight = product.description;
                            dish_product.product = String.valueOf(product.id);
                            NewReceptActivity.dish.product.add(dish_product);
                        }
                        Toast.makeText(getActivity(), "Продукты сохранены для рецента", Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(getActivity(),"Необходимо авторизоваться",Toast.LENGTH_LONG).show();
                }
            }
        });

        return v;
    }
}