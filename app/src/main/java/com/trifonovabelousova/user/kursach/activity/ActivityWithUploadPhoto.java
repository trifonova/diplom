package com.trifonovabelousova.user.kursach.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.trifonovabelousova.user.kursach.fragment.profile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ActivityWithUploadPhoto extends AppCompatActivity {

    protected static final int REQUEST_AUTH=1991;
    protected FirebaseUser user=null;

    public void reload() {
        FirebaseAuth.getInstance().getCurrentUser().reload();
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }


    @SuppressWarnings("VisibleForTests")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        if (requestCode == REQUEST_AUTH) {
            if (resultCode == -REQUEST_AUTH) {
                Toast.makeText(getApplicationContext(),"Вам ограничен доступ к приложению",Toast.LENGTH_LONG).show();
                reload();
            }
        }
        else{
            switch (requestCode) {
                case profile.CAMERA_RESULT:
                    if (profile.fileUri!=null) {
                        // showToast("Вы выбрали фотографию!");
                        Uri selectedImage = profile.fileUri;

                        Bitmap avatarImage=null;
                        try {
                            avatarImage = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if(avatarImage!=null) {
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            avatarImage.compress(Bitmap.CompressFormat.JPEG, 30, baos);
                            byte[] dataNew = baos.toByteArray();

                            FirebaseStorage storage = FirebaseStorage.getInstance();
                            StorageReference storageRef = storage.getReference();


                            StorageReference mountainImagesRef = storageRef.child("user").child(user.getUid());
                            UploadTask uploadTask = mountainImagesRef.putBytes(dataNew);
                            uploadTask.addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle unsuccessful uploads
                                }
                            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    try {
                                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                                .setPhotoUri(downloadUrl)
                                                .build();

                                        user.updateProfile(profileUpdates)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            reload();
                                                        } else {
                                                            Toast.makeText(ActivityWithUploadPhoto.this, "Ошибка при сохранении данных. Проверьте активность Интернет соединения!", Toast.LENGTH_LONG).show();
                                                        }
                                                    }
                                                });
                                    }
                                    catch (Exception e){

                                    }
                                }
                            });
                        }
                    }
                    break;

                case profile.CHOOSE_RESULT:
                    if (data != null) {
                        // showToast("Вы выбрали фотографию!");
                        Uri selectedImage = data.getData();

                        Bitmap avatarImage =null;
                        try {
                            avatarImage = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if(avatarImage!=null) {
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            avatarImage.compress(Bitmap.CompressFormat.JPEG, 30, baos);
                            byte[] dataNew = baos.toByteArray();

                            FirebaseStorage storage = FirebaseStorage.getInstance();
                            StorageReference storageRef = storage.getReference();

                            StorageReference mountainImagesRef = storageRef.child("user").child(user.getUid());
                            UploadTask uploadTask = mountainImagesRef.putBytes(dataNew);
                            uploadTask.addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle unsuccessful uploads
                                }
                            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    try {
                                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                                .setPhotoUri(downloadUrl)
                                                .build();

                                        user.updateProfile(profileUpdates)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            reload();
                                                        } else {
                                                            Toast.makeText(ActivityWithUploadPhoto.this, "Ошибка при сохранении данных. Проверьте активность Интернет соединения!", Toast.LENGTH_LONG).show();
                                                        }
                                                    }
                                                });
                                    }
                                    catch (Exception e){

                                    }
                                }
                            });
                        }
                    }
                    else {
                        Toast.makeText(this,"Вы не выбрали фотографию",Toast.LENGTH_LONG).show();
                    }

                    break;
            }
        }
    }

      public ActivityWithUploadPhoto getActivity(){
        return this;
      }
}
