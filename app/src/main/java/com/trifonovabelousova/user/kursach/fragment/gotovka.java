package com.trifonovabelousova.user.kursach.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.User_setting;
import com.trifonovabelousova.user.kursach.activity.description;
import com.trifonovabelousova.user.kursach.adapter.BaseAdapter;
import com.trifonovabelousova.user.kursach.adapter.GotovkaListAdapter;
import com.trifonovabelousova.user.kursach.firebase_object.Dish;
import com.trifonovabelousova.user.kursach.utils.FileUtils;

import java.util.ArrayList;
import java.util.List;

public class gotovka extends Fragment {

    RecyclerView rv;
    FirebaseDatabase database;
    DatabaseReference reference;
    List<Dish> listProducts=new ArrayList<>();
    GotovkaListAdapter gotovkaListAdapter;
    User_setting us;
    ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
            // try{
            Dish d = dataSnapshot.getValue(Dish.class);

                listProducts.add(d);
                gotovkaListAdapter = new GotovkaListAdapter(listProducts);
                gotovkaListAdapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener<Dish>() {
                    @Override
                    public void onItemClick(Dish item, int position) {
                        Intent intent=new Intent(getActivity(), description.class);
                        intent.putExtra("id",item.id);
                        startActivity(intent);
                    }
                });
                rv.setAdapter(gotovkaListAdapter);

            // }
            // catch (Exception e){
            //
            // }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            listProducts.remove(dataSnapshot.getValue(Dish.class));
            gotovkaListAdapter=new GotovkaListAdapter(listProducts);
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };


    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b) {
        View v = li.inflate(R.layout.fragment_gotovka, vg, false);

        us=User_setting.getInstance();
        rv = (RecyclerView) v.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        gotovkaListAdapter=new GotovkaListAdapter(listProducts);
        rv.setAdapter(gotovkaListAdapter);

        database = FirebaseDatabase.getInstance();
        reference = database.getReference("dish");


        Query recentPostsQuery = reference.limitToLast(100); //	Устанавливает максимальное количество элементов для возврата из конца упорядоченного списка результатов

        recentPostsQuery.addChildEventListener(childEventListener);

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem search = menu.findItem(R.id.action_search);
        SearchView sv=(SearchView) MenuItemCompat.getActionView(search);
        sv.setQueryHint("Поиск...");
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                gotovkaListAdapter.search(newText.toLowerCase());
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_photo:


                break;
            default:
                break;
        }

        return false;
    }
}