package com.trifonovabelousova.user.kursach.utils;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.trifonovabelousova.user.kursach.User_setting;
import com.trifonovabelousova.user.kursach.firebase_object.MyProduct;
import com.trifonovabelousova.user.kursach.firebase_object.NeedProduct;
import com.trifonovabelousova.user.kursach.firebase_object.Product;


/**
 * пример работы с сервисом
 * @
 * @see //https://github.com/googlesamples/android-play-location/blob/master/LocationUpdatesForegroundService/app/src/main/java/com/google/android/gms/location/sample/locationupdatesforegroundservice/MainActivity.java
 * @see //https://github.com/googlesamples/android-play-location/blob/master/LocationUpdatesForegroundService/app/src/main/java/com/google/android/gms/location/sample/locationupdatesforegroundservice/LocationUpdatesService.java
 */
public class ProductService extends Service {
    private static final String PACKAGE_NAME =
            "com.trifonovabelousova.user.kursach.utils";

    private static final String TAG = "tagx";

    public static final int NOTIFICATION_LOCATION_ID = 0;
    private final IBinder mBinder = new LocalBinder();
    private Handler mServiceHandler;
    private User_setting user_setting=null;
    protected int count=0;
    DatabaseReference reference_product;
    NotificationUtils notificationUtils;
    protected int need_buy=0;
    /**
     * The current location.
     */
    protected String result="";

    public ProductService() {
    }

    @Override
    public void onCreate() {
        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        mServiceHandler = new Handler(handlerThread.getLooper());
        user_setting=User_setting.getInstance();
        notificationUtils= NotificationUtils.getInstance(getApplicationContext(),getApplicationContext().NOTIFICATION_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                NeedProduct needProduct=dataSnapshot.getValue(NeedProduct.class);
                boolean have=false;
                try {
                    for (MyProduct p : user_setting.getMyProducts()) {
                        if (needProduct.id_product == p.id_product) {
                            have = true;
                            if (Float.parseFloat(needProduct.weight) > Float.parseFloat(p.weight)) {
                                need_buy++;
                            }
                        }
                    }
                }
                catch (Exception e){

                }
                if(!have){
                    need_buy++;
                }
                count++;
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser user=mAuth.getCurrentUser();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        if (user!=null) {
            //user.reload();

            DatabaseReference reference = database.getReference("user_need_product").child(user.getUid());
            reference_product = database.getReference("product");

            Query recentPostsQuery = reference.limitToLast(100); //	Устанавливает максимальное количество элементов для возврата из конца упорядоченного списка результатов
            recentPostsQuery.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.i(dataSnapshot.getKey(),dataSnapshot.getChildrenCount()+"Count");
                    if(count==dataSnapshot.getChildrenCount() && need_buy>0){
                        notificationUtils.getmNotificationManager().notify(NOTIFICATION_LOCATION_ID,
                                notificationUtils.withSound(true).withVibrate(true)
                                        .getNotification("Что приготовить?","В вашем холодильнике есть ряд продуктов, которые заканчиваются!"));

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
            recentPostsQuery.addChildEventListener(childEventListener);
        }

        // Tells the system to not try to recreate the service after it has been killed.
        return START_STICKY;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) comes to the foreground
        // and binds with this service. The service should cease to be a foreground service
        // when that happens.
        Log.i(TAG, "in onBind()");
        stopForeground(true);
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) returns to the foreground
        // and binds once again with this service. The service should cease to be a foreground
        // service when that happens.
        Log.i(TAG, "in onRebind()");
        stopForeground(true);
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(TAG, "Last client unbound from service");
        return true; // Ensures onRebind() is called when a client re-binds.
    }

    @Override
    public void onDestroy() {
        Log.d("LocationService", "QuestActivity onServiceDisconnected");
        mServiceHandler.removeCallbacksAndMessages(null);
    }

    public class LocalBinder extends Binder {
        public ProductService getService() {
            return ProductService.this;
        }
    }
}
