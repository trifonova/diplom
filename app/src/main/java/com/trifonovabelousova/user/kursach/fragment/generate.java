package com.trifonovabelousova.user.kursach.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.User_setting;
import com.trifonovabelousova.user.kursach.activity.description;
import com.trifonovabelousova.user.kursach.adapter.BaseAdapter;
import com.trifonovabelousova.user.kursach.adapter.GotovkaListAdapter;
import com.trifonovabelousova.user.kursach.firebase_object.Dish;
import com.trifonovabelousova.user.kursach.firebase_object.Dish_product;
import com.trifonovabelousova.user.kursach.firebase_object.MyProduct;

import java.util.ArrayList;
import java.util.List;

public class generate extends Fragment {

    RecyclerView rv;
    FirebaseDatabase database;
    DatabaseReference reference;
    List<Dish> listProducts=new ArrayList<>();
    GotovkaListAdapter gotovkaListAdapter;
    User_setting us;
    ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
            // try{
            Dish d = dataSnapshot.getValue(Dish.class);
            boolean cancook = true;
            for (Dish_product product: d.product) {
                boolean haveproduct = false;
                for (MyProduct myProduct: User_setting.getInstance().getMyProducts()){
                    try {
                        if (myProduct.id_product == Long.parseLong(product.product)) {
                            haveproduct = true;
                            if (Double.parseDouble(myProduct.weight) < Double.parseDouble(product.weight))
                                cancook = false;
                            break;
                        }
                    }
                    catch (Exception e){}
            }
            if (!haveproduct)
                cancook = false;
        }

            if (cancook) {
                listProducts.add(d);
                gotovkaListAdapter = new GotovkaListAdapter(listProducts);
                gotovkaListAdapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener<Dish>() {
                    @Override
                    public void onItemClick(Dish item, int position) {
                        Intent intent=new Intent(getActivity(), description.class);
                        intent.putExtra("id",item.id);
                        startActivity(intent);
                    }
                });
                rv.setAdapter(gotovkaListAdapter);
            }
            // }
            // catch (Exception e){
            //
            // }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            listProducts.remove(dataSnapshot.getValue(Dish.class));
            gotovkaListAdapter=new GotovkaListAdapter(listProducts);
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b) {
        View v = li.inflate(R.layout.fragment_gotovka, vg, false);

        us=User_setting.getInstance();
        rv = (RecyclerView) v.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        gotovkaListAdapter=new GotovkaListAdapter(listProducts);
        rv.setAdapter(gotovkaListAdapter);

        database = FirebaseDatabase.getInstance();
        reference = database.getReference("dish");


        Query recentPostsQuery = reference.limitToLast(100); //	Устанавливает максимальное количество элементов для возврата из конца упорядоченного списка результатов

        recentPostsQuery.addChildEventListener(childEventListener);

        return v;
    }
}