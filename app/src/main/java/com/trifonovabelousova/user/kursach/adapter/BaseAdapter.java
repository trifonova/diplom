package com.trifonovabelousova.user.kursach.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.trifonovabelousova.user.kursach.R;

import java.util.List;



public abstract class BaseAdapter<T, VH extends BaseAdapter.ViewHolder> extends RecyclerView.Adapter<VH> implements View.OnClickListener {

    List<T> source;
    OnItemClickListener mOnItemClickListener;//храним обработчик
    OnItemRemoveListener mOnItemRemoveListener;//храним обработчик нажатий

    public BaseAdapter(List<T> source) {
        this.source = source;
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        holder.itemView.setTag(R.id.recyclerItem, getItem(position));
    }

    @Override
    public final VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = onCreateView(parent, viewType);
        itemView.setOnClickListener(this);
        return onCreateViewHolder(itemView);
    }

    public int getItemPosition(T item) {
        return source.indexOf(item);
    }

    public void addItem(T item) {
        source.add(item);
        notifyDataSetChanged();
    }

    public void removeItem(T item) {
        source.remove(item);
        notifyDataSetChanged();
    }

    public abstract View onCreateView(ViewGroup parent, int viewType);

    public abstract VH onCreateViewHolder(View itemView);

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    public void setOnItemRemoveListener(OnItemRemoveListener onItemRemoveListener) {
        this.mOnItemRemoveListener = onItemRemoveListener;
    }

    public T getItem(int position) {
        if (position <= source.size())
            return source.get(position);
        else
            return null;
    }

    @Override
    public int getItemCount() {
        return source.size();
    }


    @Override
    public void onClick(View v) {       //когда нажали объект адаптера
        T item = (T) v.getTag(R.id.recyclerItem);
        if (mOnItemClickListener != null) { //если есть обработчик нажатия - то запускаем его
            mOnItemClickListener.onItemClick(item, getItemPosition(item));
        }
    }

    public interface OnItemClickListener<T> {//интерфейс работы нажатия

        void onItemClick(T item, int position);
    }

    public interface OnItemRemoveListener<T> {//интерфейс работы нажатия

        void onItemRemove(T item, int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        View itemView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
        }
    }
}
