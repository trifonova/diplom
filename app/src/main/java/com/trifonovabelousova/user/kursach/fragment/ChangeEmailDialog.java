package com.trifonovabelousova.user.kursach.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.activity.ActivityWithUploadPhoto;
import com.trifonovabelousova.user.kursach.activity.MainActivity;


public class ChangeEmailDialog extends SimpleDialogFragment {
    View dialogView = null;
    public static String TAG = "ChangeEmailDialog";
    FirebaseUser user=null;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public Builder build(Builder builder) {
        user = FirebaseAuth.getInstance().getCurrentUser();
        dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.change_line, null);

        final EditText value = (EditText) dialogView.findViewById(R.id.value);
        TextView title=(TextView)dialogView.findViewById(R.id.type_line);
        title.setText(getString(R.string.text_email));
        if(user==null)
            return  builder.setView(dialogView);
        value.setText(user.getEmail());

        dialogView.findViewById(R.id.but_change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(value.getText().length()>0)
                user.updateEmail(value.getText().toString())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                ChangeEmailDialog.this.dismiss();
                                ((ActivityWithUploadPhoto) getActivity()).reload();
                            }
                            else {
                                try {
                                    Toast.makeText(getActivity(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                }
                                catch (Exception e){

                                }
                            }
                        }
                    });
            }
        });

        dialogView.findViewById(R.id.but_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeEmailDialog.this.dismiss();
            }
        });
        builder.setView(dialogView);
        return builder;
    }

}