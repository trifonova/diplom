package com.trifonovabelousova.user.kursach.firebase_object;

/**
 * Блюдо
 */

import java.util.ArrayList;


public class Dish {

    public long id; //идентификатор для выбора и поиска
    public ArrayList<String> photo; //фотоографии блюда : ссылки
    public ArrayList<String> video; //видео блюда : ссылки
    public String title; //название блюда: окрошка
    public String description; //описание
    public ArrayList<Cooking_part> cooking; //Шаги по приготовлению
    public String time; //время приготовления: мин
    public ArrayList<Dish_product> product; //Продукты и вес необходимые для приготовления
    //public ArrayList<Comment> comments; //Комментарии пользователей
    //public ArrayList<Rating> ratings; //рейтинг блюда - рейтинг каждово пользователя
    //public User user; //автор - пользователь
    public String calories; //калории

    public Dish() {
    }

    public Dish(ArrayList photo, String title, String  time, String calories, ArrayList product, String description, ArrayList cooking) {
        this.photo = photo;
        this.title = title;
        this.time = time;
        this.calories = calories;
        this.product = product;
        this.description = description;
        this.cooking = cooking;
    }

}