/*package com.trifonovabelousova.user.kursach.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.trifonovabelousova.user.kursach.Item.ProductItem;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.activity.WithConnectRealm2;
import com.trifonovabelousova.user.kursach.activity.pokupki2;
import com.trifonovabelousova.user.kursach.adapter.Adapter2;

import java.util.List;

public class pokupki extends WithConnectRealm2 {

    RecyclerView rv;
    FloatingActionButton plus;
    Adapter2 adapter;
    FloatingActionButton button;


    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b) {
        View v = li.inflate(R.layout.pokupki, vg, false);

        rv = (RecyclerView) v.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        button = (FloatingActionButton) v.findViewById(R.id.button);
        plus = (FloatingActionButton) v.findViewById(R.id.plus);

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent zad = new Intent(getActivity(), pokupki2.class);
                startActivity(zad);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (ProductItem item : adapter.select) {
                    realm.beginTransaction();
                    realm.where(ProductItem.class).equalTo("name", item.getName()).findAll().deleteAllFromRealm();
                    realm.commitTransaction();
                }
                loadFormRealm();
                Toast.makeText(getActivity(), "Продукты удалены ", Toast.LENGTH_SHORT).show();
            }
        });
        loadFormRealm();

        return v;
    }

    void loadFormRealm() {
        List<ProductItem> cl = realm.where(ProductItem.class).findAll();
        adapter = new Adapter2(cl,realm,false);
        rv.setAdapter(adapter);
    }
}
*/