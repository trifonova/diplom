package com.trifonovabelousova.user.kursach.adapter;

import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.firebase_object.Product;

import java.util.List;

public class ProductHaveListAdapter extends BaseAdapter<Product, ProductHaveListAdapter.RouteHolder> {

    View.OnClickListener funDelete = null;

    public ProductHaveListAdapter(List<Product> source) {
        super(source);
    }
    public ProductHaveListAdapter(List<Product> source,View.OnClickListener funDelete) {
        super(source);
        this.funDelete=funDelete;
    }

    @Override
    public RouteHolder onCreateViewHolder(View itemView) {
        return new RouteHolder(itemView);
    }

    @Override
    public View onCreateView(ViewGroup parent, int viewType) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_delete, parent, false);
    }

    @Override
    public void onBindViewHolder(RouteHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        Product item= getItem(position);
        holder.t_title.setText(item.title);
        holder.t_ves.setText(item.description);
        holder.t_type.setText(item.type_weight.shorttitle);
        holder.delete.setTag(item);
        holder.delete.setOnClickListener(funDelete);


        Glide.with(holder.i_photo.getContext())
                .load(item.photo)
                .into(holder.i_photo);

    }

    class RouteHolder extends BaseAdapter.ViewHolder {
        View itemView;
        TextView t_title;
        TextView t_ves;
        ImageView i_photo;
        FloatingActionButton delete;
        TextView t_type;

        public RouteHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            t_title=(TextView)itemView.findViewById(R.id.title);
            t_ves=(TextView)itemView.findViewById(R.id.ves);
            i_photo=(ImageView) itemView.findViewById(R.id.photo);
            t_type=(TextView)itemView.findViewById(R.id.type_weight);
            delete=(FloatingActionButton) itemView.findViewById(R.id.delete);
        }

    }

    public void removeItemId(long id) {
        for(Product product:source){
            if(product.id==id){
                source.remove(product);
                break;
            }
        }
        notifyDataSetChanged();
    }
}