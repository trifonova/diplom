package com.trifonovabelousova.user.kursach.firebase_object;

import com.trifonovabelousova.user.kursach.firebase_object.Category;
import com.trifonovabelousova.user.kursach.firebase_object.Type_weight;

/**
 * Продукты - рис, молоко, сметана
 */

public class Product {

    public long id; //идентификатор для выбора и поиска
    public String photo; //фото продукта: ссылка
    public String title; //название продукта: молоко
    public String description; //описание - не обязательно
    public Category category; //Категория
    public Type_weight type_weight; //тип веса

    public Product() {
    }

    public Product(String photo, String title, Category category, Type_weight type_weight) {
        this.photo = photo;
        this.title = title;
        this.category = category;
        this.type_weight = type_weight;
    }
}

