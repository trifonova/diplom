package com.trifonovabelousova.user.kursach.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.fragment.ChangeEmailDialog;
import com.trifonovabelousova.user.kursach.fragment.ChangeFioDialog;
import com.trifonovabelousova.user.kursach.utils.FileUtils;

import java.io.File;
import java.util.Calendar;

import static android.provider.MediaStore.ACTION_IMAGE_CAPTURE;

public class ProfileActivity extends ActivityWithUploadPhoto {


    public static Uri fileUri=null;
    private static final String TAG = "PROFILE_FRAGMENT";
    public static final int MY_PERMISSIONS_REQUEST_CAMERA=51;


    public static final int CAMERA_RESULT = 0;
    public static final int CHOOSE_RESULT = 1;

    public static final int MEDIA_TYPE_IMAGE = 1;

    //FragmentProfileBinding mBinding;
    FirebaseUser user=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Профиль");
        final TextView tvProfileName= (TextView)findViewById(R.id.tvProfileName);
        final TextView email= (TextView)findViewById(R.id.email);
        final ImageView civAvatar= (ImageView) findViewById(R.id.header_logo);
        Button buttonDeliver= (Button) findViewById(R.id.buttonDeliver);
        FrameLayout changeFio = (FrameLayout) findViewById(R.id.changeFio);
        FrameLayout changeEmail = (FrameLayout) findViewById(R.id.changeEmail);

        user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null)
        {
            user =  FirebaseAuth.getInstance().getCurrentUser();

            tvProfileName.setText(user.getDisplayName());
            email.setText(user.getEmail());
            Glide.with(this)
                    .load(user.getPhotoUrl())
                    .centerCrop()
                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                    .into(civAvatar);




            buttonDeliver.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FirebaseAuth mAuth = FirebaseAuth.getInstance();
                    mAuth.signOut();

                    reload();
                    //Перезагрузка активности
                    //((MainActivity) getActivity()).reload();
                }
            });

            //изменение имени
            changeFio.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    ChangeFioDialog dialog=new ChangeFioDialog();
                    dialog.show(getSupportFragmentManager(),"Измнеить ФИО");
                    return false;
                }
            });

            changeEmail.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    ChangeEmailDialog dialog=new ChangeEmailDialog();
                    dialog.show(getSupportFragmentManager(),"Измнеить E_MAIL");
                    return false;
                }
            });

        }


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photoDialog();
            }
        });
    }




    private Uri getOutputMediaFileUri(int type){
        return  FileProvider.getUriForFile(this, getPackageName() + ".provider",getOutputMediaFile(type));
    }

    private File getOutputMediaFile(int type){
        File file = new File(FileUtils.getFilesDir(), Calendar.getInstance().getTimeInMillis()+".jpg");
        return file;
    }


    public void startCamera(){
        Intent takePicture = new Intent(ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE); // create a file to save the image
        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(takePicture, CAMERA_RESULT);
    }

    public void photoDialog() {
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(this);
        alertDialog.setTitle("Задание новой аватарки");
        alertDialog.setMessage("Откуда будем брать изображение?");
        alertDialog.setNegativeButton(getString(R.string.text_camera), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (ContextCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    startCamera();
                }
                else{
                    ActivityCompat.requestPermissions(ProfileActivity.this,
                            new String[]{Manifest.permission.CAMERA},
                            MY_PERMISSIONS_REQUEST_CAMERA);
                }
            }
        });
        alertDialog.setCancelable(true);
        alertDialog.setPositiveButton(getString(R.string.text_gallery), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (ContextCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, CHOOSE_RESULT);
                }
                else{
                    ActivityCompat.requestPermissions(ProfileActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_CAMERA);
                }
            }
        });
        alertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
