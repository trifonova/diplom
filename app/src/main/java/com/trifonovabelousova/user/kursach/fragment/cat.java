package com.trifonovabelousova.user.kursach.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.adapter.AdapterForCategory;
import com.trifonovabelousova.user.kursach.adapter.BaseAdapter;
import com.trifonovabelousova.user.kursach.firebase_object.Category;
import com.trifonovabelousova.user.kursach.fragment.productvibor;

import java.util.ArrayList;
import java.util.List;

public class cat extends Fragment {

    RecyclerView rv;
    AdapterForCategory adapter;
    List<Category> cl = new ArrayList<>();
    FirebaseDatabase database;
    DatabaseReference reference;
    int regim=0; //0 - выбор, что имеем в холодильнике, 1 - выбор, что должно быть в холодильнике

    ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
            // try{
            cl.add(dataSnapshot.getValue(Category.class));
            adapter = new AdapterForCategory(cl);
            adapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener<Category>() {
                @Override
                public void onItemClick(Category item, int position) {
                    Bundle b=new Bundle();
                    b.putLong("id",item.getId());
                    b.putInt("regim",regim);
                    productvibor p=new productvibor();
                    p.setArguments(b);
                   getActivity().getSupportFragmentManager().beginTransaction().add(R.id.frame, p, "text").addToBackStack(null).commit();
                }
            });
            rv.setAdapter(adapter);
            // }
            // catch (Exception e){
            //
            // }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            cl.remove(dataSnapshot.getValue(Category.class));
            adapter=new AdapterForCategory(cl);

            //переход

            adapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener<Category>() {
                @Override
                public void onItemClick(Category item, int position) {
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame, new productvibor(), "text").commit();
                }
            });
            rv.setAdapter(adapter);
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_product, container, false);

        rv = (RecyclerView) v.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        if (getArguments()!=null) {
            regim = getArguments().getInt("regim", 0);
        }

        adapter=new AdapterForCategory(cl);
        rv.setAdapter(adapter);

        database = FirebaseDatabase.getInstance();
        reference = database.getReference("category");


        Query recentPostsQuery = reference.limitToLast(100); //	Устанавливает максимальное количество элементов для возврата из конца упорядоченного списка результатов

        recentPostsQuery.addChildEventListener(childEventListener);

        return v;
    }

}