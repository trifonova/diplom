package com.trifonovabelousova.user.kursach.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.firebase_object.Dish_product;
import com.trifonovabelousova.user.kursach.firebase_object.Product;

import java.util.ArrayList;

/**
 * Created by user on 03.02.2018.
 */

public class IngredientAdapter extends ArrayAdapter<Dish_product>{

    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Dish_product> objects;
    FirebaseDatabase database;
    DatabaseReference reference;

    public IngredientAdapter(Context context, ArrayList<Dish_product> objects) {
        super(context,R.layout.item_ingredient);
        ctx = context;
        this.objects = objects;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        database = FirebaseDatabase.getInstance();
    }

    // кол-во элементов
    @Override
    public int getCount() {
        return objects.size();
    }

    // элемент по позиции
    @Override
    public Dish_product getItem(int position) {
        return objects.get(position);
    }

    // id по позиции
    @Override
    public long getItemId(int position) {
        return position;
    }

    // пункт списка
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // используем созданные, но не используемые view
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item_ingredient, parent, false);
        }

        final Dish_product p = getItem(position);

        // заполняем View в пункте списка данными из товаров: наименование, цена
        // и картинка
        final View finalView = view;
        database.getReference("product")
                .child(p.product)
                .getRef()
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        try{

                            Product product  = snapshot.getValue(Product.class);

                            ((TextView) finalView.findViewById(R.id.ingredient)).setText(product.title);
                            ((TextView) finalView.findViewById(R.id.weight)).setText(p.weight+" "+product.type_weight.shorttitle);
                         }
                         catch (Exception e){

                         }
                    }
                    @Override public void onCancelled(DatabaseError error) { }
                });
        return view;
    }
}