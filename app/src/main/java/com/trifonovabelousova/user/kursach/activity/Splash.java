package com.trifonovabelousova.user.kursach.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.User_setting;
import com.trifonovabelousova.user.kursach.firebase_object.MyProduct;

public class Splash extends Activity {

    FirebaseDatabase database;
    DatabaseReference reference;
    DatabaseReference reference_product;
    private User_setting user_setting=null;
    private int count = 0;
    private FirebaseAuth mAuth; //переменная для того чтобы получить текущего пользователя
    private FirebaseUser user; //переменная для того чтобы получить текущего пользователя

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        user_setting=User_setting.getInstance();
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                MyProduct myProduct=dataSnapshot.getValue(MyProduct.class);
                user_setting.addMyProducts(myProduct);
                count++;
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

            mAuth = FirebaseAuth.getInstance();
            user=mAuth.getCurrentUser();
            database = FirebaseDatabase.getInstance();
            if (user!=null) {
                //user.reload();

                reference = database.getReference("user_product").child(user.getUid());
                reference_product = database.getReference("product");


                Query recentPostsQuery = reference.limitToLast(100); //	Устанавливает максимальное количество элементов для возврата из конца упорядоченного списка результатов
                recentPostsQuery.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.i(dataSnapshot.getKey(),dataSnapshot.getChildrenCount()+"Count");
                        if(dataSnapshot.getChildrenCount()==0 || count==dataSnapshot.getChildrenCount()){
                            Intent i = new Intent(Splash.this, Registration.class);
                            startActivity(i);
                            finish();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.i("",""+"Count");
                    }
                });
                recentPostsQuery.addChildEventListener(childEventListener);
            }
            else {
                Intent i = new Intent(Splash.this, Registration.class);
                startActivity(i);
                finish();
            }
    }
}