package com.trifonovabelousova.user.kursach.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.trifonovabelousova.user.kursach.R;

/**
 * Created by Andrej_Maligin on 12.04.2018.
 */

public class NotificationUtils {


    private static final NotificationUtils ourInstance = new NotificationUtils();
    private Context context=null;
    private NotificationManager mNotificationManager=null;
    private boolean withSound=false;
    public static final String CHANNEL_ID = "channel_01";
    private boolean withVibrate=false;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public NotificationManager getmNotificationManager() {
        return mNotificationManager;
    }

    public void setmNotificationManager(NotificationManager mNotificationManager) {
        this.mNotificationManager = mNotificationManager;
    }


    public static NotificationUtils getInstance(Context context, String NOTIFICATION_SERVICE) {
        ourInstance.mNotificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        ourInstance.context = context;
       // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
       //     CharSequence name = context.getString(R.string.app_name);

            //NotificationChannel mChannel =
            //        new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);

           // ourInstance.mNotificationManager.createNotificationChannel(mChannel);
       // }

        return ourInstance;
    }

    public NotificationUtils withSound(boolean withSound) {
        this.withSound = withSound;
        return this;
    }

    public NotificationUtils withVibrate(boolean withSound) {
        this.withSound = withSound;
        return this;
    }

    public Notification getNotification(String title, String message) {
        return getNotification(title,message,null);
    }


    public Notification getNotification(String title, String message, PendingIntent servicePendingIntent) {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setContentText(message)
                .setContentTitle(title);
                //.setOngoing(true)

        if(withVibrate)
                builder=builder.setVibrate(new long[] { 1000, 1000});
        if (withSound)
            builder=builder.setSound(alarmSound);

        builder=builder.setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(servicePendingIntent);
        //.setWhen(System.currentTimeMillis());

        return builder.build();
    }


    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }

}
