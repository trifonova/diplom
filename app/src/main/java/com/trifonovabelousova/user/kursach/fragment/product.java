package com.trifonovabelousova.user.kursach.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.adapter.ProductListAdapter;
import com.trifonovabelousova.user.kursach.firebase_object.Product;

import java.util.ArrayList;
import java.util.List;

public class product extends Fragment {

    RecyclerView rv;
    FirebaseDatabase database;
    DatabaseReference reference;
    List<Product> listProducts=new ArrayList<>();
    ProductListAdapter productListAdapter;
    long id;

    ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
           // try{
            if (dataSnapshot.getValue(Product.class).category.id==id || id==-1) {
                listProducts.add(dataSnapshot.getValue(Product.class));
                productListAdapter = new ProductListAdapter(listProducts);
                rv.setAdapter(productListAdapter);
            }
           // }
           // catch (Exception e){
           //
           // }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            listProducts.remove(dataSnapshot.getValue(Product.class));
            productListAdapter=new ProductListAdapter(listProducts);
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b) {
        View v = li.inflate(R.layout.fragment_product, vg, false);

        rv = (RecyclerView) v.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        if (getArguments()!=null) {
            id = getArguments().getLong("id", 0);
        } else id=-1;

        productListAdapter=new ProductListAdapter(listProducts);
        rv.setAdapter(productListAdapter);

        database = FirebaseDatabase.getInstance();
        reference = database.getReference("product");


        Query recentPostsQuery = reference.limitToLast(100); //	Устанавливает максимальное количество элементов для возврата из конца упорядоченного списка результатов

        recentPostsQuery.addChildEventListener(childEventListener);

        return v;
    }
}
