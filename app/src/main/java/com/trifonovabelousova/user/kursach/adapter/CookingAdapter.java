package com.trifonovabelousova.user.kursach.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.firebase_object.Cooking_part;

import java.util.ArrayList;

public class CookingAdapter extends ArrayAdapter<Cooking_part>{

    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Cooking_part> objects;
    ArrayList<Cooking_part> selected;

    public CookingAdapter(Context context, ArrayList<Cooking_part> objects) {
        super(context,R.layout.item_cooking);
        ctx = context;
        this.objects = objects==null?new ArrayList<Cooking_part>():objects;
        this.selected = new ArrayList<>(this.objects);
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void selectItems(String str) {
        selected = new ArrayList<>();
        for (Cooking_part s:objects) {
//            if (s.title){
//                selected.add(s);
//            }
        }
        notifyDataSetChanged();
    }
    // кол-во элементов
    @Override
    public int getCount() {
        return selected.size();
    }

    // элемент по позиции
    @Override
    public Cooking_part getItem(int position) {
        return objects.get(position);
    }

    // id по позиции
    @Override
    public long getItemId(int position) {
        return position;
    }

    // пункт списка
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // используем созданные, но не используемые view
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item_cooking, parent, false);
        }

        final Cooking_part cookingPart = getItem(position);

        ((TextView) view.findViewById(R.id.title)).setText(cookingPart.title);
        //((TextView) view.findViewById(R.id.time)).setText(cookingPart.time);

        return view;
    }
}