package com.trifonovabelousova.user.kursach.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.trifonovabelousova.user.kursach.R;

public class polzovatel extends AppCompatActivity {

    private FirebaseAuth mAuth;
    EditText imya;
    Button change;
    Button nazad;
    FirebaseUser user=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.polzovatel);

        mAuth = FirebaseAuth.getInstance();

        change = (Button) findViewById(R.id.change);
        nazad = (Button) findViewById(R.id.nazad);
        imya = (EditText) findViewById(R.id.imya);

        user = mAuth.getCurrentUser();

        change.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                    .setDisplayName(imya.getText().toString())
                    .build();

            user.updateProfile(profileUpdates)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                polzovatel.this.finish();
                            }
                            else{
                                Toast.makeText(polzovatel.this,"Ошибка при сохранении данных. Проверьте активность Интернет соединения",Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
    });

        nazad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent info = new Intent(polzovatel.this, MainActivity.class);
                startActivity(info);
                finish();
            }
        });
    }
}