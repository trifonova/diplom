package com.trifonovabelousova.user.kursach;

import com.trifonovabelousova.user.kursach.firebase_object.MyProduct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 30.01.2018.
 */

public class User_setting {
    private static final User_setting ourInstance = new User_setting();
    private List<MyProduct> myProducts=new ArrayList<>();
    public static User_setting getInstance() {
        return ourInstance;
    }

    private User_setting() {
    }

    public List<MyProduct> getMyProducts() {
        return myProducts;
    }

    public void setMyProducts(List<MyProduct> myProducts) {
        this.myProducts = myProducts;
    }

    public void addMyProducts(MyProduct myProduct) {
        this.myProducts.add(myProduct);
    }
}
