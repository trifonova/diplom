package com.trifonovabelousova.user.kursach.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.activity.ActivityWithUploadPhoto;
import com.trifonovabelousova.user.kursach.activity.MainActivity;


public class ChangePasswordDialog extends SimpleDialogFragment {
    View dialogView = null;
    FirebaseUser user=null;
    public static String TAG = "ChangePasswordDialog";

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public Builder build(Builder builder) {
        user = FirebaseAuth.getInstance().getCurrentUser();
        dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.change_password, null);
        if(user==null)
            return  builder.setView(dialogView);

        final EditText password = (EditText) dialogView.findViewById(R.id.pass);

        final EditText password_repeat = (EditText) dialogView.findViewById(R.id.two_pass);

        dialogView.findViewById(R.id.but_change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(password.getText().toString().equals(password_repeat.getText().toString())) {
                    if (password.getText().length() > 5) {
                        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                .setDisplayName(password.getText().toString())
                                .build();

                        user.updateProfile(profileUpdates)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            ChangePasswordDialog.this.dismiss();
                                            ((ActivityWithUploadPhoto) getActivity()).reload();
                                        } else {
                                            Toast.makeText(getActivity(), "Ошибка при сохранении данных. Проверьте активность Интернет соединения!", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                    }
                }
                else {
                    Toast.makeText(getActivity(), "Пароли не совпадают!", Toast.LENGTH_LONG).show();
                }
            }
        });

        dialogView.findViewById(R.id.but_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePasswordDialog.this.dismiss();
            }
        });
        builder.setView(dialogView);
        return builder;
    }

}