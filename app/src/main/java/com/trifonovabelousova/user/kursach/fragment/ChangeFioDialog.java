package com.trifonovabelousova.user.kursach.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.activity.ActivityWithUploadPhoto;
import com.trifonovabelousova.user.kursach.activity.MainActivity;


public class ChangeFioDialog extends SimpleDialogFragment {
    View dialogView = null;
    public static String TAG = "ChangeFioDialog";
    FirebaseUser user=null;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }


    @Override
    public Builder build(Builder builder) {
        user = FirebaseAuth.getInstance().getCurrentUser();
        dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.change_fio, null);
        final EditText surname = (EditText) dialogView.findViewById(R.id.surname);
        if(user!=null) {
            surname.setText(user.getDisplayName());
        }

        dialogView.findViewById(R.id.but_change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text=surname.getText().toString();
                if (surname.getText().length() > 0) {
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setDisplayName(surname.getText().toString())
                            .build();

                    user.updateProfile(profileUpdates)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        ChangeFioDialog.this.dismiss();
                                        ((ActivityWithUploadPhoto) getActivity()).reload();
                                    } else {
                                        Toast.makeText(getActivity(), "Ошибка при сохранении данных. Проверьте активность Интернет соединения!", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                }
            }
        });

        dialogView.findViewById(R.id.but_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeFioDialog.this.dismiss();
            }
        });
        builder.setView(dialogView);
        return builder;
    }

}