package com.trifonovabelousova.user.kursach.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.trifonovabelousova.user.kursach.R;

public class Vxod extends AppCompatActivity implements View.OnClickListener{

    private FirebaseAuth mAuth; //переменная для того чтобы получить текущего пользователя
    private FirebaseAuth.AuthStateListener mAuthListener;
    private EditText Emael;
    private EditText Password;
    private Button vhod;
    private Button knopka;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vxod);

        mAuth = FirebaseAuth.getInstance();
        vhod = (Button)findViewById(R.id.vhod);
        knopka = (Button)findViewById(R.id.knopka);

        knopka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent zad = new Intent(Vxod.this, Registration.class);
                startActivity(zad);
                finish();
            }
        });

        //слушатель состояние  текущего нашего пользователя
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Intent info = new Intent(Vxod.this, MainActivity.class);
                    startActivity(info);

                } else {
                    // User is signed out
                }
            }
        };

        Emael = (EditText) findViewById(R.id.et_email);
        Password = (EditText) findViewById(R.id.et_password);

//если пользователь уже авторизован
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            Intent info= new Intent(Vxod.this, MainActivity.class);
            startActivity(info);
            finish();
        }
        vhod.setOnClickListener(this);
    }

    public void onClick(View v) {
            sign(Emael.getText().toString(),Password.getText().toString());
    }

    public void sign(String email , String password) {
        mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()) {
                    Toast.makeText(Vxod.this, "Вход выполнен", Toast.LENGTH_SHORT).show();
                    Intent info= new Intent(Vxod.this,MainActivity.class);
                    startActivity(info);
                    finish();
                }
                else
                    Toast.makeText(Vxod.this, "Не удалось войти", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
