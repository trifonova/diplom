package com.trifonovabelousova.user.kursach.firebase_object;

/**
 * Содержит связь между продуктом и весом необходимым в блюде
 */

public class Dish_product {

    public long id; //идентификатор для выбора и поиска
    public String product; //продукт
    public String weight; //выбранный вес
    public int sort_id; //номер в сортировке - не обязательно

    public Dish_product() {
    }

    public Dish_product(String product, String weight) {
        this.product = product;
        this.weight = weight;
    }
}