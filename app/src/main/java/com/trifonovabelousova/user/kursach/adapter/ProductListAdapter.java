package com.trifonovabelousova.user.kursach.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.firebase_object.Product;

import java.util.List;

public class ProductListAdapter extends BaseAdapter<Product, ProductListAdapter.RouteHolder> {

    public ProductListAdapter(List<Product> source) {
        super(source);
    }

    @Override
    public RouteHolder onCreateViewHolder(View itemView) {
        return new RouteHolder(itemView);
    }

    @Override
    public View onCreateView(ViewGroup parent, int viewType) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
    }

    @Override
    public void onBindViewHolder(RouteHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        Product item= getItem(position);
        holder.t_title.setText(item.title);
        holder.t_category.setText(item.category.getTitle());
        holder.t_type.setText(item.type_weight.title);


        Glide.with(holder.i_photo.getContext())
                .load(item.photo)
                .into(holder.i_photo);

    }

    class RouteHolder extends BaseAdapter.ViewHolder {
        View itemView;
        TextView t_title;
        TextView t_category;
        ImageView i_photo;
        TextView t_type;

        public RouteHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            t_title=(TextView)itemView.findViewById(R.id.title);
            t_category=(TextView)itemView.findViewById(R.id.category);
            i_photo=(ImageView) itemView.findViewById(R.id.photo);
            t_type=(TextView)itemView.findViewById(R.id.type_weight);
        }

    }

}