package com.trifonovabelousova.user.kursach.activity;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.adapter.InstructionAdapter;
import com.trifonovabelousova.user.kursach.firebase_object.Cooking_part;
import com.trifonovabelousova.user.kursach.firebase_object.Dish;
import com.trifonovabelousova.user.kursach.fragment.cat;

import java.util.ArrayList;

public class NewReceptActivity extends AppCompatActivity {

    private static final int PRODUCT_RESULT = 998;
    InstructionAdapter instructionAdapter;
    public static Dish dish=null;
    FirebaseDatabase database;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_recept);

        database = FirebaseDatabase.getInstance();

        RecyclerView listView=(RecyclerView) findViewById(R.id.list_instruction);
        FloatingActionButton plus_instruction=(FloatingActionButton)findViewById(R.id.plus_instruction);
        Button but_add=(Button)findViewById(R.id.but_add);
        Button but_add_products=(Button)findViewById(R.id.but_add_products);
        final TextView instruction=(TextView) findViewById(R.id.instruction);
        final TextView title=(TextView) findViewById(R.id.title);
        final TextView image_link=(TextView) findViewById(R.id.image_link);
        final TextView desc=(TextView) findViewById(R.id.desc);
        final TextView calories=(TextView) findViewById(R.id.calories);
        final TextView time=(TextView) findViewById(R.id.time);

        listView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        instructionAdapter=new InstructionAdapter(new ArrayList<Cooking_part>());
        listView.setAdapter(instructionAdapter);
        plus_instruction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cooking_part cookingPart=new Cooking_part();
                cookingPart.id=System.currentTimeMillis();
                cookingPart.title=instruction.getText().toString();
                instructionAdapter.addItem(cookingPart);
                instruction.setText("");
            }
        });
        but_add_products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(NewReceptActivity.this,SelectCategoryActivity.class);
                startActivityForResult(intent,PRODUCT_RESULT);
            }
        });
        but_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reference = database.getReference("dish");
                dish.id=System.currentTimeMillis();
                dish.title=title.getText().toString();
                ArrayList<String> list_image=new ArrayList<>();
                list_image.add(image_link.getText().toString());
                dish.photo=list_image;
                dish.description=desc.getText().toString();
                dish.calories=calories.getText().toString();
                dish.time=time.getText().toString();
                dish.cooking=new ArrayList<>(instructionAdapter.getItems());
                reference.child(String.valueOf(dish.id)).setValue(dish);
                finish();
            }
        });
        ViewCompat.setNestedScrollingEnabled(listView, true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
}
