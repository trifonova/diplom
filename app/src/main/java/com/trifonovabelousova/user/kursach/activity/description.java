package com.trifonovabelousova.user.kursach.activity;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.adapter.CookingAdapter;
import com.trifonovabelousova.user.kursach.adapter.IngredientAdapter;
import com.trifonovabelousova.user.kursach.firebase_object.Dish;
import com.trifonovabelousova.user.kursach.firebase_object.Dish_product;
import com.trifonovabelousova.user.kursach.utils.ListViewFixed;

import java.util.ArrayList;

public class description extends AppCompatActivity {

    ImageView image;
    TextView name;
    TextView des;
    TextView ingredient;
    TextView time;
    TextView calories;
    CollapsingToolbarLayout collapsingToolbar;
    ArrayList<Dish_product> products = new ArrayList<Dish_product>();
    IngredientAdapter ingredientAdapter;
    CookingAdapter cookingAdapter;
    FirebaseDatabase database;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.description);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);


        image = (ImageView) findViewById(R.id.image);
        name = (TextView) findViewById(R.id.name);
        ingredient = (TextView) findViewById(R.id.ingredient);
        des = (TextView) findViewById(R.id.des);
        time = (TextView) findViewById(R.id.time);
        calories = (TextView) findViewById(R.id.calories);


        database = FirebaseDatabase.getInstance();
        database.getReference("dish")
                .child(String.valueOf(getIntent().getLongExtra("id",1)))
                .getRef()
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //try{

                    Dish recept  = snapshot.getValue(Dish.class);

                    if(recept.photo.size()>0)
                        Glide
                                .with(description.this)
                                .load(recept.photo.get(0))
                                .into(image);

                    name.setText(recept.title);
                    collapsingToolbar.setTitle(recept.title);
                    //ingredient.setText(recept.product);
                    des.setText(recept.description);
                    time.setText(recept.time);
                    calories.setText(recept.calories);


                    // создаем адаптер
                    ingredientAdapter = new IngredientAdapter(description.this,  recept.product);

                    // настраиваем список
                    ListView list_products = (ListView) findViewById(R.id.list_products);
                    list_products.setAdapter(ingredientAdapter);
                    ListViewFixed.setListViewHeightBasedOnChildren(list_products);


                    // создаем адаптер
                    cookingAdapter = new CookingAdapter(description.this,  recept.cooking);

                    ListView list_cooking = (ListView) findViewById(R.id.list_cooking);
                    list_cooking.setAdapter(cookingAdapter);
                    ListViewFixed.setListViewHeightBasedOnChildren(list_cooking);
                // }
                // catch (Exception e){
                //
                // }
            }
            @Override public void onCancelled(DatabaseError error) { }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}