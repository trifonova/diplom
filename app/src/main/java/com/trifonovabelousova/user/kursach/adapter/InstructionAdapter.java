package com.trifonovabelousova.user.kursach.adapter;

import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.firebase_object.Cooking_part;
import com.trifonovabelousova.user.kursach.firebase_object.Dish;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrej_Maligin on 01.04.2018.
 */

public class InstructionAdapter  extends com.trifonovabelousova.user.kursach.adapter.BaseAdapter<Cooking_part, InstructionAdapter.RouteHolder> {

    public InstructionAdapter(List<Cooking_part> source) {
        super(source);
    }

    @Override
    public InstructionAdapter.RouteHolder onCreateViewHolder(View itemView) {
        return new InstructionAdapter.RouteHolder(itemView);
    }

    @Override
    public View onCreateView(ViewGroup parent, int viewType) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.item_instruction, parent, false);
    }

    @Override
    public void onBindViewHolder(InstructionAdapter.RouteHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        Cooking_part item= getItem(position);
        holder.text.setText(item.title);
        holder.del.setTag(item);
        holder.del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cooking_part p = (Cooking_part)view.getTag();
                source.remove(p);
                notifyDataSetChanged();
            }
        });
    }

    public List<Cooking_part> getItems() {
        return source;
    }

    class RouteHolder extends BaseAdapter.ViewHolder {
        View itemView;
        TextView text;
        FloatingActionButton del;

        public RouteHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            text=(TextView)itemView.findViewById(R.id.text);
            del=(FloatingActionButton)itemView.findViewById(R.id.del);
        }
    }

    public void addItem(Cooking_part cookingPart) {
        source.add(cookingPart);
        notifyDataSetChanged();
    }
}