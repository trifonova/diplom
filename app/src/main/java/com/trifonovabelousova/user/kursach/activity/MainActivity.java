package com.trifonovabelousova.user.kursach.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.firebase_object.Dish;
import com.trifonovabelousova.user.kursach.fragment.generate;
import com.trifonovabelousova.user.kursach.fragment.gotovka;
import com.trifonovabelousova.user.kursach.fragment.korzina;
import com.trifonovabelousova.user.kursach.fragment.producthave;
import com.trifonovabelousova.user.kursach.fragment.settings;
import com.trifonovabelousova.user.kursach.utils.ProductService;

public class MainActivity extends ActivityWithUploadPhoto implements NavigationView.OnNavigationItemSelectedListener {

    private static final int DISH_RESULT = 967;
    TextView name;

    public void reload() {
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();

        //if (user!= null) {
        //    Toast.makeText(MainActivity.this, user.getDisplayName(), Toast.LENGTH_SHORT).show();
        //}

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer,toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav);
        navigationView.setNavigationItemSelectedListener(this);

        ImageView image = (ImageView)navigationView.getHeaderView(0).findViewById(R.id.profile_image);
        View profile = navigationView.getHeaderView(0);
        if(user!=null) {
            ((TextView)profile.findViewById(R.id.fio)).setText(user.getDisplayName());
            ((TextView)profile.findViewById(R.id.email)).setText(user.getEmail());
            Glide.with(this)
                    .load(user.getPhotoUrl())
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(image);
            profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent=new Intent(MainActivity.this,ProfileActivity.class);
                    startActivity(intent);
//                    getSupportFragmentManager().beginTransaction().replace(R.id.frame, new profile(), "text").commit();
//                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//                    drawer.closeDrawer(GravityCompat.START);
                }
            });
        }
        try {
            Intent serviceIntent = new Intent(this, ProductService.class);
            startService(serviceIntent);
        }
        catch (Exception e){

        }
        getSupportFragmentManager().beginTransaction().add(R.id.frame, new producthave(), "text").commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_empty, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

               if (id == R.id.my_product) {
                   getSupportFragmentManager().beginTransaction().replace(R.id.frame, new producthave(), "text").commit();

               } else if (id == R.id.gotovka) {
                   getSupportFragmentManager().beginTransaction().replace(R.id.frame, new generate(), "text").commit();

               } else if (id == R.id.all_recept) {
                   getSupportFragmentManager().beginTransaction().replace(R.id.frame, new gotovka(), "text").commit();

               } else if (id == R.id.recept) {

                   Intent intent=new Intent(MainActivity.this,NewReceptActivity.class);
                   NewReceptActivity.dish=new Dish();
                   startActivityForResult(intent,DISH_RESULT);

                   //getSupportFragmentManager().beginTransaction().replace(R.id.frame, new recept(), "text").commit();

               } else if (id == R.id.korzina) {
                   getSupportFragmentManager().beginTransaction().replace(R.id.frame, new korzina(), "text").commit();

               } else if (id == R.id.settings) {
                   getSupportFragmentManager().beginTransaction().replace(R.id.frame, new settings(), "text").commit();
//                   Intent intent=new Intent(MainActivity.this,ProfileActivity.class);
//                   startActivity(intent);

               } else if (id == R.id.exit) {

            AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
            dialog.setTitle("Выход");
            dialog.setMessage("Вы действительно хотите выйти из приложения?");

            dialog.setPositiveButton("Да",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            MainActivity.this.finish();
                        }
                    });

            dialog.setNegativeButton("Нет",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            dialog.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        NewReceptActivity.dish=null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            Intent serviceIntent = new Intent(this, ProductService.class);
            stopService(serviceIntent);
        }
        catch (Exception e){

        }
    }
}
