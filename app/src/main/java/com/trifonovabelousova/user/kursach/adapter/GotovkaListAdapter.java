package com.trifonovabelousova.user.kursach.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.firebase_object.Dish;

import java.util.ArrayList;
import java.util.List;

public class GotovkaListAdapter extends BaseAdapter<Dish, GotovkaListAdapter.RouteHolder> {

    List<Dish> all=new ArrayList<>();

    public GotovkaListAdapter(List<Dish> source) {
        super(source);
        all=new ArrayList<>(source);
    }

    @Override
    public RouteHolder onCreateViewHolder(View itemView) {
        return new RouteHolder(itemView);
    }

    @Override
    public View onCreateView(ViewGroup parent, int viewType) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gotovka, parent, false);
    }

    @Override
    public void onBindViewHolder(RouteHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        Dish item= getItem(position);
        holder.t_title.setText(item.title);
        holder.calories.setText(String.valueOf(item.calories));
        holder.desc.setText(item.description);


        Glide.with(holder.i_photo.getContext())
                .load(item.photo.get(0))
                .centerCrop()
                .into(holder.i_photo);
    }

    public void search(String newText) {
        source=new ArrayList<>();
        for(Dish d:all){
            if(d.title.toLowerCase().contains(newText) || d.description.toLowerCase().contains(newText)){
                source.add(d);
            }
        }
        notifyDataSetChanged();
    }

    class RouteHolder extends BaseAdapter.ViewHolder {
        View itemView;
        TextView t_title;
        TextView calories;
        ImageView i_photo;
        TextView desc;

        public RouteHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            t_title=(TextView)itemView.findViewById(R.id.title);
            calories=(TextView)itemView.findViewById(R.id.calories);
            i_photo=(ImageView) itemView.findViewById(R.id.photo);
            desc=(TextView)itemView.findViewById(R.id.desc);
        }
    }
}