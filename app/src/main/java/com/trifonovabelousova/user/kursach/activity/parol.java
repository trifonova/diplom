package com.trifonovabelousova.user.kursach.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.trifonovabelousova.user.kursach.R;

public class parol extends AppCompatActivity {

    EditText pass;
    Button change;
    Button nazad;

    FirebaseUser user=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parol);

        change = (Button) findViewById(R.id.change);
        nazad = (Button) findViewById(R.id.nazad);
        pass = (EditText) findViewById(R.id.pass);

        change.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            FirebaseAuth auth = FirebaseAuth.getInstance();

            auth.sendPasswordResetEmail(pass.getText().toString())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(parol.this, "Пароль отправлен на Вашу почту", Toast.LENGTH_SHORT).show();
                                FirebaseAuth mAuth = FirebaseAuth.getInstance();
                                mAuth.signOut();

                                Intent info = new Intent(parol.this, Vxod.class);
                                startActivity(info);
                            }
                        }
                    });
        }
        });
        nazad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent info = new Intent(parol.this, MainActivity.class);
                startActivity(info);
                finish();
            }
        });
    }
}