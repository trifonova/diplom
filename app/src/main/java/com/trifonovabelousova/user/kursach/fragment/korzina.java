package com.trifonovabelousova.user.kursach.fragment;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.User_setting;
import com.trifonovabelousova.user.kursach.adapter.ProductNeedListAdapter;
import com.trifonovabelousova.user.kursach.firebase_object.MyProduct;
import com.trifonovabelousova.user.kursach.firebase_object.NeedProduct;
import com.trifonovabelousova.user.kursach.firebase_object.Product;

import java.util.ArrayList;
import java.util.List;

public class korzina extends Fragment {

    RecyclerView rv;
    FloatingActionButton pl;
    FirebaseDatabase database;
    DatabaseReference reference;
    DatabaseReference reference_product;
    List<Product> listProducts=new ArrayList<>();
    ProductNeedListAdapter productListAdapter;
    private User_setting user_setting=null;
    private FirebaseAuth mAuth; //переменная для того чтобы получить текущего пользователя
    private FirebaseUser user; //переменная для того чтобы получить текущего пользователя

    ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
            final NeedProduct needProduct=dataSnapshot.getValue(NeedProduct.class);
            boolean have=false;
            for (MyProduct p : user_setting.getMyProducts()) {
                if (needProduct.id_product == p.id_product) {
                    have = true;
                    if (Float.parseFloat(needProduct.weight) > Float.parseFloat(p.weight)) {
                        final float razn=Float.parseFloat(needProduct.weight)-Float.parseFloat(p.weight);
                        reference_product.child(String.valueOf(needProduct.id_product)).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Product product=dataSnapshot.getValue(Product.class);
                                product.description=String.valueOf(razn);
                                productListAdapter.addItem(product);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                }
            }
            if(!have){
                final float razn=Float.parseFloat(needProduct.weight);
                reference_product.child(String.valueOf(needProduct.id_product)).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Product product=dataSnapshot.getValue(Product.class);
                        product.description=String.valueOf(razn);
                        productListAdapter.addItem(product);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b) {
        View v = li.inflate(R.layout.fragment_product, vg, false);

        rv = (RecyclerView) v.findViewById(R.id.rv);
        user_setting=User_setting.getInstance();

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        productListAdapter=new ProductNeedListAdapter(listProducts, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        rv.setAdapter(productListAdapter);

        mAuth = FirebaseAuth.getInstance();
        user=mAuth.getCurrentUser();
        database = FirebaseDatabase.getInstance();
        if (user == null)
            return v;
        reference = database.getReference("user_need_product").child(user.getUid());
        reference_product = database.getReference("product");


        Query recentPostsQuery = reference.limitToLast(100); //	Устанавливает максимальное количество элементов для возврата из конца упорядоченного списка результатов

        recentPostsQuery.addChildEventListener(childEventListener);

        return v;
    }
}