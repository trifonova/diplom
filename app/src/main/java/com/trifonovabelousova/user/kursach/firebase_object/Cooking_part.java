package com.trifonovabelousova.user.kursach.firebase_object;

/**
 * один шаг по приготовлению блюда - например поставте кострюлю с водой на плитну на 20 минут
 */

public class Cooking_part {

    public long id; //идентификатор для выбора и поиска
    public String title; //описание процесса
    public String photo; //картинка - о том что это - варка, жарка, резка, духовка (картинки лучше png  с прозрачным фоном)
    public String time; //время процесса в мин

    public Cooking_part() {
    }
}
