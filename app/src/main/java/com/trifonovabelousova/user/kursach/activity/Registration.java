package com.trifonovabelousova.user.kursach.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.trifonovabelousova.user.kursach.R;

public class Registration extends AppCompatActivity{

    EditText login;
    EditText password;
    Button save;
    Button vhod;
    private FirebaseAuth mAuth; //переменная для того чтобы получить текущего пользователя
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);

        login = (EditText) findViewById(R.id.login);
        password = (EditText) findViewById(R.id.password);
        save = (Button) findViewById(R.id.save);
        vhod = (Button)findViewById(R.id.vhod);
        mAuth = FirebaseAuth.getInstance();
        //слушатель состояние  текущего нашего пользователя

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {

                } else {
                }
            }
        };
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registration(login.getText().toString(),password.getText().toString());
            }
        });

        vhod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sign(login.getText().toString(),password.getText().toString());
            }
        });


        //если пользователь уже авторизован
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            Intent info= new Intent(Registration.this, MainActivity.class);
            startActivity(info);
            finish();
        }
    }


    public void registration (String email , String password){
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful())
                {
                    Toast.makeText(Registration.this, "Регистрация выполнена", Toast.LENGTH_SHORT).show();
                    Intent info = new Intent(Registration.this, MainActivity.class);
                    startActivity(info);
                    finish();
                }
                else
                    Toast.makeText(Registration.this, "Не удалось зарегистрироваться", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void sign(String email , String password) {
        mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()) {
                    Toast.makeText(Registration.this, "Вход выполнен", Toast.LENGTH_SHORT).show();
                    Intent info= new Intent(Registration.this,MainActivity.class);
                    startActivity(info);
                    finish();
                }
                else
                    Toast.makeText(Registration.this, "Не удалось войти", Toast.LENGTH_SHORT).show();
            }
        });
    }
}