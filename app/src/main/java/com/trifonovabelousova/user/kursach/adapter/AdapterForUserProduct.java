package com.trifonovabelousova.user.kursach.adapter;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.trifonovabelousova.user.kursach.R;
import com.trifonovabelousova.user.kursach.firebase_object.MyProduct;
import com.trifonovabelousova.user.kursach.firebase_object.Product;

import java.util.ArrayList;
import java.util.List;

public class AdapterForUserProduct extends BaseAdapter<Product, AdapterForUserProduct.RouteHolder> {
    List<Product> select=new ArrayList<>();

    public AdapterForUserProduct(List<Product> source) {
        super(source);
    }

    @Override
    public RouteHolder onCreateViewHolder(View itemView) {
        return new RouteHolder(itemView);
    }

    @Override
    public View onCreateView(ViewGroup parent, int viewType) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_check, parent, false);
    }

    @Override
    public void onBindViewHolder(RouteHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        final Product item= getItem(position);
        holder.t_title.setText(item.title);
        holder.t_type.setText(item.type_weight.shorttitle);
        item.description="0";
        holder.ch.setTag(item);
        holder.ves.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                item.description=editable.toString();
            }
        });

        if(select.contains(item)){
            holder.ch.setChecked(true);
        }
        else{
            holder.ch.setChecked(false);
        }
        holder.ch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (!select.contains((Product) compoundButton.getTag())) {
                        select.add((Product) compoundButton.getTag());
                    }
                } else {
                    select.remove((Product) compoundButton.getTag());
                }
            }
        });


        Glide.with(holder.i_photo.getContext())
                .load(item.photo)
                .into(holder.i_photo);

    }

    public void addHave(Product value) {
    }

    public List<Product> getSelect() {
        return select;
    }

    class RouteHolder extends BaseAdapter.ViewHolder {
        View itemView;
        TextView t_title;
        TextView ves;
        ImageView i_photo;
        TextView t_type;
        CheckBox ch;

        public RouteHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            t_title=(TextView)itemView.findViewById(R.id.title);
            ves=(TextView)itemView.findViewById(R.id.ves);
            i_photo=(ImageView) itemView.findViewById(R.id.photo);
            t_type=(TextView)itemView.findViewById(R.id.type_weight);
            ch = (CheckBox) itemView.findViewById(R.id.ch);
        }

    }

}