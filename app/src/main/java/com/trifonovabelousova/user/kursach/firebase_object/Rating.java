package com.trifonovabelousova.user.kursach.firebase_object;

import com.trifonovabelousova.user.kursach.firebase_object.User;

/**
 * Рейтинг поставленный пользователем
 */

public class Rating {

    public User user;
    public float val;

}